﻿using System.Text.Json;

public class StudentManagement
{
    AppDbContext _dbContext;
    public StudentManagement()
    {
        _dbContext = new AppDbContext();
        SeedData();
    }

    public List<Student> GetStudents()
    {
        List<Student> students = _dbContext.Students.ToList();
        return students;
    }

    public Student GetStudentById(int id)
    {
        return _dbContext.Students.Find(id);
    }
    public void AddNewStudent(Student student)
    {
        _dbContext.Students.Add(student);
        _dbContext.SaveChanges();
    }

    public void UpdateStudent(Student student)
    {
        _dbContext.Students.Update(student);
        _dbContext.SaveChanges();
    }

    public void RemoveStudent(int id)
    {
        Student student = _dbContext.Students.Find(id);
        if (student != null)
        {
            _dbContext.Students.Remove(student);
            _dbContext.SaveChanges();
        }
    }

    void SeedData()
    {
        if (_dbContext.Students.Any())
        { // Kiểm tra có dữ liệu hay chưa
            return;
        }
        List<Student> students;
        // Đọc dữ liệu từ file json hay bất cứ nguồn nào
        using (StreamReader sr = new StreamReader("MOCK_DATA.json"))
        {
            string studentJson = sr.ReadToEnd();
            students = JsonSerializer.Deserialize<List<Student>>(studentJson);
        }
        // context.Persons.Addrange(...)
        if (students != null)
        {
            _dbContext.Students.AddRange(students);
        }
        // Lưu dữ liệu
        _dbContext.SaveChanges();

    }
}

