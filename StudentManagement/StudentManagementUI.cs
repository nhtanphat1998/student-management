﻿public class StudentManagementUI
{
    StudentManagement _studentManagement;
    public StudentManagementUI()
    {
        _studentManagement = new StudentManagement();
    }
    
    public void GetAllStudent()
    {
        List<Student> students = _studentManagement.GetStudents();
        foreach (Student student in students)
        {
            Console.WriteLine($"Student {student.Id} - {student.Name} - {student.Age} - {student.Major}");
        }
    }
    public void AddNewStudent()
    {
        try
        {
            Console.WriteLine("--------Add New Student-------");
            Console.WriteLine("Enter Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Enter Age");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Major");
            string major = Console.ReadLine();
            Student student = new Student()
            {
                Name = name,
                Age = age,
                Major = major
            };
            _studentManagement.AddNewStudent(student);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public void UpdateStudent()
    {
        try
        {
            Console.WriteLine("--------Update New Student-------");
            Console.WriteLine("Enter Id:");
            int id = int.Parse(Console.ReadLine());
            Student student = _studentManagement.GetStudentById(id);
            if (student != null)
            {
                Console.WriteLine("Enter Name:");
                student.Name = Console.ReadLine();
                Console.WriteLine("Enter Age");
                student.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter Major");
                student.Major = Console.ReadLine();
                _studentManagement.UpdateStudent(student);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public void RemoveStudent()
    {
        try
        {
            Console.WriteLine("--------Remove New Student-------");
            Console.WriteLine("Enter Id:");
            int id = int.Parse(Console.ReadLine());
            _studentManagement.RemoveStudent(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}

