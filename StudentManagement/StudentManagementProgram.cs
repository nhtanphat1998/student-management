﻿public class StudentManagementProgram
{
    StudentManagementUI ui = new StudentManagementUI();
    public void Menu()
    {
        Console.WriteLine("-----------Student Management------------");
        Console.WriteLine("1. Add new student");
        Console.WriteLine("2. Show list students");
        Console.WriteLine("3. Update student");
        Console.WriteLine("4. Remove student");
        Console.WriteLine("5. Quit");
        Console.Write("Choose your option: ");
    }
    public void Start()
    {
        while (true)
        {
            Menu();

            string choice = Console.ReadLine();
            Console.WriteLine();

            switch (choice)
            {
                case "1":
                    ui.AddNewStudent();
                    break;
                case "2":
                    ui.GetAllStudent();
                    break;
                case "3":
                    ui.UpdateStudent();
                    break;
                case "4":
                    ui.RemoveStudent();
                    break;
                case "5":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng nhập một lựa chọn hợp lệ.");
                    break;
            }

            Console.WriteLine();
        }
    }
}
