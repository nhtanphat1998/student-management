﻿using Microsoft.EntityFrameworkCore;
public class AppDbContext : DbContext
{
    public DbSet<Student> Students { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("Data Source=DESKTOP-KF6RCOI;Initial Catalog=StudentManagement;Persist Security Info=True;User ID=sa;Password=phat1234;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Student>()
                    .Property(x => x.Id)
                    .UseIdentityColumn(1, 1);
                    
    }
}

